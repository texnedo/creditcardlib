package com.texnedo.api;

import com.texnedo.api.CheckResult.Status;
import com.texnedo.data.VendorData;
import com.texnedo.data.VendorPatternLoader;
import com.texnedo.utils.LruCache;
import com.texnedo.utils.TextUtils;
import com.texnedo.utils.Utils;

import java.util.List;
import java.util.regex.Matcher;

import static com.texnedo.api.CheckResult.LENGTH_NOT_VALID_RESULT;
import static com.texnedo.api.CheckResult.SYMBOLS_NOT_VALID_RESULT;

public class ValidateCardApiImpl implements ValidateCardApi {
    private static final int MIN_CARD_NUMBER_LENGTH = 12;
    private static final int MAX_CARD_NUMBER_LENGTH = 19;
    private static final int CARD_NUMBER_PREFIX = 6;

    private static final int MAX_CHECKS_CACHE = 100;
    private static final int MAX_VENDOR_CACHE = 50;

    private final VendorPatternLoader loader;
    private final LruCache<String, CheckResult> checksCache;
    private final LruCache<String, String> vendorCache;

    ValidateCardApiImpl(VendorPatternLoader loader) {
        this.loader = loader;
        this.checksCache = new LruCache<>(MAX_CHECKS_CACHE);
        this.vendorCache = new LruCache<>(MAX_VENDOR_CACHE);
    }

    @Override
    public CheckResult validate(String cardNumber) {
        if (cardNumber == null) {
            return LENGTH_NOT_VALID_RESULT;
        }
        CheckResult result = checksCache.get(cardNumber);
        if (result != null) {
            return result;
        }
        result = validateInternal(cardNumber);
        checksCache.put(cardNumber, result);
        return result;
    }

    private CheckResult validateInternal(String cardNumber) {
        if (TextUtils.isEmpty(cardNumber) || cardNumber.length() < CARD_NUMBER_PREFIX) {
            return LENGTH_NOT_VALID_RESULT;
        }
        if (!checkCardNumberData(cardNumber)) {
            return SYMBOLS_NOT_VALID_RESULT;
        }
        final String vendorName = getCardVendorName(cardNumber);
        if (cardNumber.length() < MIN_CARD_NUMBER_LENGTH ||
                cardNumber.length() > MAX_CARD_NUMBER_LENGTH) {
            return new CheckResult(vendorName, Status.LENGTH_NOT_VALID);
        }
        if (!Utils.passLuhnCheck(cardNumber)) {
            return new CheckResult(vendorName, Status.INTEGRITY_CHECK_FAILED);
        }
        return new CheckResult(vendorName, Status.OK);
    }

    private static boolean checkCardNumberData(String cardNumber) {
        if (TextUtils.isEmpty(cardNumber)) {
            return false;
        }
        for (int i = 0; i < cardNumber.length(); i++) {
            char item = cardNumber.charAt(i);
            if (!Character.isDigit(item)) {
                return false;
            }
        }
        return true;
    }

    private String getCardVendorName(String cardNumber) {
        final String prefix = cardNumber.substring(0, CARD_NUMBER_PREFIX);
        final List<VendorData> data = loader.getData();
        String vendorName = vendorCache.get(prefix);
        if (vendorName != null) {
            return vendorName;
        }
        for (VendorData item : data) {
            final Matcher matcher = item.cardNumberPattern.matcher(prefix);
            matcher.region(0, CARD_NUMBER_PREFIX);
            if (matcher.matches()) {
                vendorName = item.name;
                break;
            }
        }
        if (vendorName != null) {
            vendorCache.put(prefix, vendorName);
        }
        return vendorName;
    }
}
