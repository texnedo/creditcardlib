package com.texnedo.api;

import com.texnedo.utils.TextUtils;

import java.util.Locale;

/**
 * Result for a particular credit card number validation.
 * */
public class CheckResult {
    private static final String UNKNOWN_CARD_VENDOR = "Unknown";
    private static final String STATUS_VALID = "valid";
    private static final String STATUS_INVALID = "invalid";

    final static CheckResult LENGTH_NOT_VALID_RESULT =
            new CheckResult(null, Status.LENGTH_NOT_VALID);

    final static CheckResult SYMBOLS_NOT_VALID_RESULT =
            new CheckResult(null, Status.SYMBOLS_NOT_VALID);

    public final String vendorName;
    public final boolean vendorDetected;
    public final Status status;

    public CheckResult(String vendorName, Status status) {
        if (TextUtils.isEmpty(vendorName)) {
            this.vendorName = UNKNOWN_CARD_VENDOR;
            this.vendorDetected = false;
        } else {
            this.vendorName = vendorName;
            this.vendorDetected = true;
        }
        this.status = status;
    }

    public boolean isValid() {
        return status == Status.OK;
    }

    public String getTextResult() {
        String textStatus;
        if (isValid()) {
            textStatus = STATUS_VALID;
        } else {
            textStatus = STATUS_INVALID;
        }
        return String.format(Locale.US, "%s, %s", textStatus, vendorName);
    }

    @Override
    public String toString() {
        return "CheckResult{" +
                "vendorName='" + vendorName + '\'' +
                ", status=" + status +
                '}';
    }

    enum Status {
        OK,
        LENGTH_NOT_VALID,
        SYMBOLS_NOT_VALID,
        INTEGRITY_CHECK_FAILED
    }
}
