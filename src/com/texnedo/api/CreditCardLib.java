package com.texnedo.api;

import com.texnedo.data.VendorPatternLoaderImpl;

/**
 * Main entry point to access library components.
 * */
public final class CreditCardLib {
    private static volatile ValidateCardApi api;

    /**
     * Factory method to create and hold library instance.
     * */
    public static ValidateCardApi getApi() {
        if (api == null) {
            synchronized (CreditCardLib.class) {
                if (api == null) {
                    api = new ValidateCardApiImpl(new VendorPatternLoaderImpl());
                }
            }
        }
        return api;
    }
}
