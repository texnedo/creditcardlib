package com.texnedo.api;

/**
 * Main library interface to check credit card number.
 * Use file vendor_data.json to add new vendors, credit card numbers or vendor priorities.
 * */
public interface ValidateCardApi {
    /**
     * Validates string with a credit card number.
     * @return check result for provided number
     * */
    CheckResult validate(String cardNumber);
}
