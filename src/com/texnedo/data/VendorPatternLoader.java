package com.texnedo.data;

import java.util.List;

public interface VendorPatternLoader {
    List<VendorData> getData();
}
