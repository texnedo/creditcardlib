package com.texnedo.data;

import com.google.gson.Gson;
import com.google.gson.JsonParseException;
import com.texnedo.utils.TextUtils;
import com.texnedo.utils.Utils;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.regex.Pattern;

public class VendorPatternLoaderImpl implements VendorPatternLoader {
    private static final String RESOURCE_FILE = "vendor_data.json";

    private volatile List<VendorData> vendorData;

    @Override
    public List<VendorData> getData() {
        if (vendorData == null) {
            synchronized (VendorPatternLoaderImpl.this) {
                if (vendorData == null) {
                    vendorData = buildVendorData();
                }
            }
        }
        return vendorData;
    }

    private List<VendorData> buildVendorData() {
        final URL jsonResource = VendorPatternLoaderImpl.class.getClassLoader()
                .getResource(RESOURCE_FILE);
        if (jsonResource == null) {
            throw new IllegalArgumentException(
                    String.format(Locale.US, "Resource file %s not found", RESOURCE_FILE)
            );
        }
        final String jsonPath = jsonResource.getPath();
        final String json;
        try {
            json = Utils.readFile(new File(jsonPath));
        } catch (IOException ex) {
            throw new IllegalArgumentException(
                    String.format(Locale.US, "Failed to read from resource file %s", RESOURCE_FILE),
                    ex
            );
        }
        if (TextUtils.isEmpty(json)) {
            //noinspection unchecked
            return Collections.EMPTY_LIST;
        }
        final ArrayList<VendorData> data = new ArrayList<>();
        final Gson gson = new Gson();
        VendorModel[] models;
        try {
            models = gson.fromJson(json, VendorModel[].class);
        } catch (JsonParseException ex) {
            throw new IllegalArgumentException(
                    String.format(Locale.US, "Failed to parse resource file %s", RESOURCE_FILE),
                    ex
            );
        }
        for (VendorModel model : models) {
            final String[] patterns = new String[model.patterns.length];
            for (int i = 0; i < patterns.length; i++) {
                patterns[i] = model.patterns[i].pattern;
            }
            data.add(build(model.name, patterns, model.priority));
        }
        Collections.sort(data);
        return data;
    }

    private static VendorData build(String name, String[] patterns, int priority) {
        final StringBuilder builder = new StringBuilder();
        builder.append("^(");
        for (String pattern : patterns) {
            if (builder.length() != 2) {
                builder.append("|");
            }
            builder.append(pattern);
        }
        builder.append(")[\\d]*");
        final Pattern unifiedPattern = Pattern.compile(builder.toString());
        return new VendorData(name, unifiedPattern, priority);
    }

}
