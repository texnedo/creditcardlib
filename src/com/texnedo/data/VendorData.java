package com.texnedo.data;

import java.util.regex.Pattern;

public class VendorData implements Comparable<VendorData> {
    public final String name;
    public final Pattern cardNumberPattern;
    private int priority;

    VendorData(String name, Pattern cardNumberPattern, int priority) {
        this.name = name;
        this.cardNumberPattern = cardNumberPattern;
        this.priority = priority;
    }

    @Override
    public int compareTo(VendorData o) {
        return -Integer.compare(priority, o.priority);
    }
}
