package com.texnedo.data;

public final class VendorModel {
    public String name;
    public int priority;
    public PatternModel[] patterns;
}
