package com.texnedo.utils;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;

public class Utils {
    public static boolean passLuhnCheck(int[] digits) {
        return getLuhnCheckSum(digits, 0) % 10 == 0;
    }

    public static boolean passLuhnCheck(String digits) {
        return getLuhnCheckSum(digits, 0) % 10 == 0;
    }

    public static char getLuhnCheckLastDigit(String digits) {
        final int[] data = new int[digits.length()];
        for (int i = 0; i < digits.length(); i++) {
            char item = digits.charAt(i);
            data[i] = Character.digit(item, 10);
        }
        return Character.forDigit(getLuhnCheckLastDigit(data), 10);
    }

    public static int getLuhnCheckLastDigit(int[] digits) {
        return (getLuhnCheckSum(digits, 1) * 9) % 10;
    }

    private static int getLuhnCheckSum(int[] digits, int shift) {
        int sum = 0;
        final int length = digits.length;
        for (int i = 0; i < length; i++) {
            // get digits in reverse order
            int digit = digits[length - i - 1];
            // every 2nd number multiply with 2
            if ((i + shift) % 2 == 1) {
                digit *= 2;
            }
            sum += digit > 9 ? digit - 9 : digit;
        }
        return sum;
    }

    private static int getLuhnCheckSum(String digits, int shift) {
        int sum = 0;
        final int length = digits.length();
        for (int i = 0; i < length; i++) {
            // get digits in reverse order
            int digit = Character.digit(digits.charAt(length - i - 1), 10);
            // every 2nd number multiply with 2
            if ((i + shift) % 2 == 1) {
                digit *= 2;
            }
            sum += digit > 9 ? digit - 9 : digit;
        }
        return sum;
    }

    public static String readFile(File file) throws IOException {
        RandomAccessFile f = null;
        try {
            f = new RandomAccessFile(file, "r");
            byte[] bytes = new byte[(int) f.length()];
            f.readFully(bytes);
            return new String(bytes, "UTF-8");
        } finally {
            if (f != null) {
                try {
                    f.close();
                } catch (IOException ignored) {
                }
            }
        }
    }
}
