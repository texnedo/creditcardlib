package com.texnedo;

import com.texnedo.api.CheckResult;
import com.texnedo.api.CreditCardLib;

import java.util.Locale;

public class Main {
    public static void main(String[] args) {
        for (int i = 0; i < args.length; i++) {
            final CheckResult result = CreditCardLib.getApi().validate(args[i]);
            System.out.println(String.format(Locale.US, "%s %s, %s",
                    args[i], result.vendorName, result.status));
        }
    }
}
