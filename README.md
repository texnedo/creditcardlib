#Credit card number evaluator
Our users have large lists of credit card numbers. They want to categorise them, based on
• the validity of the card number,
• the brand that the credit number belongs to.

##Your task: 
Design and implement a Java library which provides an API to easily do these operations. Details of the API design are up to you.

#Assume that:
• a credit card number is valid if:
o it is between 12-19 digits long
o the last digit passes the Luhn check algorithm
(https://en.wikipedia.org/wiki/Luhn_algorithm)

• a credit card number belongs to a brand if the first 6 digits of the card number fall in
the following ranges:
o MasterCard
▪ 222100-272099
▪ 510000-559999
o Maestro
▪ 500000-509999
▪ 520000-530000 (has priority over MasterCard)
o Visa
▪ 400000-499999
o Keep in mind, that you might want to add support for other ranges and/or
brands in the future.

#Sample test data:
4929804463622139: valid, Visa
4929804463622138: invalid, Visa
5512132012291762: valid, MasterCard
5212132012291762: invalid, Maestro