package com.texnedo.api;

import com.texnedo.data.VendorPatternLoader;
import com.texnedo.data.VendorPatternLoaderImpl;

import java.util.Random;

import static org.junit.Assert.*;

public class ValidateCardApiImplTest {
    private static final long CARD_NUMBER_BASE = 1000000000000000L;

    @org.junit.Test
    public void validateTestSet() {
        VendorPatternLoader loader = new VendorPatternLoaderImpl();
        ValidateCardApi api = new ValidateCardApiImpl(loader);
        CheckResult result1 = api.validate("4929804463622139");
        assertEquals(result1.vendorName, "Visa");
        assertEquals(result1.status, CheckResult.Status.OK);
        assertTrue(result1.vendorDetected);
        CheckResult result2 = api.validate("4929804463622138");
        assertEquals(result2.vendorName, "Visa");
        assertEquals(result2.status, CheckResult.Status.INTEGRITY_CHECK_FAILED);
        assertTrue(result2.vendorDetected);
        CheckResult result3 = api.validate("5512132012291762");
        assertEquals(result3.vendorName, "MasterCard");
        assertEquals(result3.status, CheckResult.Status.OK);
        assertTrue(result3.vendorDetected);
        CheckResult result4 = api.validate("5212132012291762");
        assertEquals(result4.vendorName, "Maestro");
        assertEquals(result4.status, CheckResult.Status.INTEGRITY_CHECK_FAILED);
        assertTrue(result4.vendorDetected);
    }

    @org.junit.Test
    public void validateTestEmpty() {
        VendorPatternLoader loader = new VendorPatternLoaderImpl();
        ValidateCardApi api = new ValidateCardApiImpl(loader);
        CheckResult result1 = api.validate("");
        assertEquals(result1.vendorName, "Unknown");
        assertEquals(result1.status, CheckResult.Status.LENGTH_NOT_VALID);
        assertFalse(result1.vendorDetected);
        CheckResult result2 = api.validate("");
        assertEquals(result2.vendorName, "Unknown");
        assertEquals(result2.status, CheckResult.Status.LENGTH_NOT_VALID);
        assertFalse(result2.vendorDetected);
        CheckResult result3 = api.validate("    ");
        assertEquals(result3.vendorName, "Unknown");
        assertEquals(result3.status, CheckResult.Status.LENGTH_NOT_VALID);
        assertFalse(result3.vendorDetected);
        CheckResult result4 = api.validate("          ");
        assertEquals(result4.vendorName, "Unknown");
        assertEquals(result4.status, CheckResult.Status.SYMBOLS_NOT_VALID);
        assertFalse(result4.vendorDetected);
    }

    @org.junit.Test
    public void validateTestExtCases() {
        VendorPatternLoader loader = new VendorPatternLoaderImpl();
        ValidateCardApi api = new ValidateCardApiImpl(loader);
        CheckResult result1 = api.validate("521213");
        assertEquals(result1.vendorName, "Maestro");
        assertEquals(result1.status, CheckResult.Status.LENGTH_NOT_VALID);
        assertTrue(result1.vendorDetected);
        CheckResult result2 = api.validate("520000");
        assertEquals(result2.vendorName, "Maestro");
        assertEquals(result2.status, CheckResult.Status.LENGTH_NOT_VALID);
        assertTrue(result2.vendorDetected);
        CheckResult result3 = api.validate("530000");
        assertEquals(result3.vendorName, "Maestro");
        assertEquals(result3.status, CheckResult.Status.LENGTH_NOT_VALID);
        assertTrue(result3.vendorDetected);
        CheckResult result4 = api.validate("540000");
        assertEquals(result4.vendorName, "MasterCard");
        assertEquals(result4.status, CheckResult.Status.LENGTH_NOT_VALID);
        assertTrue(result4.vendorDetected);
        CheckResult result5 = api.validate("400000");
        assertEquals(result5.vendorName, "Visa");
        assertEquals(result5.status, CheckResult.Status.LENGTH_NOT_VALID);
        assertTrue(result5.vendorDetected);
    }

    @org.junit.Test
    public void validateTestInvalidSymbols() {
        VendorPatternLoader loader = new VendorPatternLoaderImpl();
        ValidateCardApi api = new ValidateCardApiImpl(loader);
        CheckResult result1 = api.validate("400000sdf");
        assertEquals(result1.vendorName, "Unknown");
        assertEquals(result1.status, CheckResult.Status.SYMBOLS_NOT_VALID);
        assertFalse(result1.vendorDetected);
        CheckResult result2 = api.validate("400000sdf");
        assertEquals(result2.vendorName, "Unknown");
        assertEquals(result2.status, CheckResult.Status.SYMBOLS_NOT_VALID);
        assertFalse(result2.vendorDetected);
    }

    @org.junit.Test
    public void validateTestStatUpTime() {
        VendorPatternLoader loader = new VendorPatternLoaderImpl();
        ValidateCardApi api = new ValidateCardApiImpl(loader);
        long t1 = System.nanoTime();
        CheckResult result1 = api.validate("4929804463622139");
        long diff1 = System.nanoTime() - t1;
        long t2 = System.nanoTime();
        CheckResult result2 = api.validate("4929804463622138");
        long diff2 = System.nanoTime() - t2;
        assertNotNull(result2);
        assertNotNull(result1);
        System.out.println(diff1);
        System.out.println(diff2);
        assertTrue((diff1 / 100) > diff2);
    }

    @org.junit.Test
    public void validateTestSameVendorCheckTime() {
        VendorPatternLoader loader = new VendorPatternLoaderImpl();
        ValidateCardApi api = new ValidateCardApiImpl(loader);
        CheckResult result1 = api.validate("111111111111111");
        long t1 = System.nanoTime();
        CheckResult result2 = api.validate("4929804463622118");
        long diff1 = System.nanoTime() - t1;
        long t2 = System.nanoTime();
        CheckResult result3 = api.validate("4929804463622138");
        long diff2 = System.nanoTime() - t2;
        assertNotNull(result1);
        assertNotNull(result2);
        assertNotNull(result3);
        System.out.println(diff1);
        System.out.println(diff2);
        assertTrue(diff1 > diff2);
    }

    @org.junit.Test
    public void validateTestSameCard() {
        VendorPatternLoader loader = new VendorPatternLoaderImpl();
        ValidateCardApi api = new ValidateCardApiImpl(loader);
        CheckResult result1 = api.validate("111111111111111");
        long t1 = System.nanoTime();
        CheckResult result2 = api.validate("4929804463622118");
        long diff1 = System.nanoTime() - t1;
        long t2 = System.nanoTime();
        CheckResult result3 = api.validate("4929804463622118");
        long diff2 = System.nanoTime() - t2;
        assertNotNull(result1);
        assertNotNull(result2);
        assertNotNull(result3);
        System.out.println(diff1);
        System.out.println(diff2);
        assertTrue((diff1 / 5) > diff2);
    }

    @org.junit.Test
    public void validateRandomCards() {
        VendorPatternLoader loader = new VendorPatternLoaderImpl();
        ValidateCardApi api = new ValidateCardApiImpl(loader);
        Random random = new Random();
        for (int i = 0; i < 100000; i++) {
            long value = random.nextLong();
            if (Long.MAX_VALUE - value < CARD_NUMBER_BASE) {
                continue;
            }
            long number = CARD_NUMBER_BASE + random.nextLong();
            CheckResult result1 = api.validate(Long.toString(number));
            assertNotNull(result1);
        }
    }
}